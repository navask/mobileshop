<?php

namespace App\Http\Controllers;

use Illuminate\Support\Str;

use Illuminate\Http\Request;
use App\Models\Product;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    // public function __construct(){
    // 	$this->middleware('auth');
    // }


    public function store(Request $request)
    {
        // Define validation rules
        $rules = [
            'code' => 'required|string|max:255|unique:products,product_code',
            'name' => 'required|string|max:255',
            'category' => 'required|string|max:255',
            'stock' => 'required|integer|min:0',
            'unit_price' => 'required|numeric|min:0',
            'sale_price' => 'required|numeric|min:0',
        ];

        // Define custom error messages
        $messages = [
            'required' => 'The :attribute field is required.',
            'string' => 'The :attribute must be a string.',
            'max' => 'The :attribute must not exceed :max characters.',
            'unique' => 'The :attribute is already in use.',
            'integer' => 'The :attribute must be an integer.',
            'numeric' => 'The :attribute must be a number.',
            'min' => 'The :attribute must be at least :min.',
        ];

        // Validate the request data
        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()->route('add.product')
                ->withErrors($validator)
                ->withInput();
        }

        // If validation passes, create and save the product
        $data = new Product;
        $data->product_code = $request->code;
        $data->name = $request->name;
        $data->category = $request->category;
        $data->stock = $request->stock;
        $data->unit_price = $request->unit_price;
        // $data->total_price = $request->stock * $request->unit_price;
        $data->sales_unit_price = $request->sale_price;
        // $data->sales_stock_price =$request->stock * $request->sale_price;

        $data->save();

        return redirect()->route('add.product')
            ->with('success', 'Product added successfully');
    }

    public function allProduct()
    {
        $products = Product::all();
        return view('Admin.all_product', compact('products'));
    }

    public function availableProducts()
    {
        $products = Product::where('stock', '>', '0')->get();
        return view('Admin.available_products', compact('products'));
    }

    public function formData($id)
    {
        $product = Product::find($id);

        return view('Admin.add_order', compact('product'));
        // return view('Admin.add_order',['product'=>$product]);
    }

    public function purchaseData($id)
    {
        $product = Product::find($id);

        return view('Admin.purchase_products', compact('product'));
    }

    public function storePurchase(Request $request)
    {

        Product::where('name', $request->name)->update(['stock' => $request->stock + $request->purchase]);

        return Redirect()->route('all.product');
    }
}
